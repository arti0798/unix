// #include "apue.h"
#include<stdio.h>
#include<stdlib.h>
// #define EOF -1
void err_sys(const char* x) 
{ 
    perror(x); 
    exit(1); 
}
int main(void) {

    int c;
    while((c = getc(stdin))!= EOF) 
        if(putc(c,stdout))
            err_sys("output error");

    if(ferror(stdin))
        err_sys("input error");

    exit(0);            
}