#include<stdio.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<unistd.h>

void main() {

    int fd;
    char message[100];
    printf("Reader....creating node\n");
    mknode("firstPipe",S_IFIFO,0);
    printf("created");
    chmod("firstPipe",0660);
    printf("opening...");
    fd = open("firstPipe",O_RDONLY);

    read(fd, message, 51);

    printf("Message to reader");
}