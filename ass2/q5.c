
#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

int isFile(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISREG(path_stat.st_mode);
}

int main(int argc, char *argv[])
{
    char *file = argv[1];
    // const char* directory = ";

    printf("Is %s a file? %s.\n", file,((isFile(file) == 1) ? "Yes" : "No"));

    printf("Is %s a directory? %s.\n", file,((isFile(file) == 0) ? "Yes" : "No"));

    return 0;
}



/*  output

arti% ./a.out q1.c
Is q1.c a file? Yes.
Is q1.c a directory? No.


arti% ./a.out ./a2AOS 
Is ./a2AOS a file? No.
Is ./a2AOS a directory? Yes.
*/