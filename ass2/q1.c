#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>

int main() {
	int fd[2]; // index 0 for read and 1 for write
	system("clear");
	if(pipe(fd) == -1) {
		printf("An error occured while opening an pipe");
		return -1;
	}
	int pid = fork();
	if(pid == 0) {
		printf("\nWe are now in child process\n");
		close(fd[0]);
		char str1[50];
		printf("Enter an message for parent process : \n");
		scanf("%[^\n]s", str1);
		getchar();
		if(write(fd[1], str1, sizeof(char[50])) == -1) { 
			printf("\nAn error occured while writing to the pipe\n");
			return -1;
		} else {
			printf("\nMessage sent to Parent!!!\n");
			printf("Child process will end here!!\n");
		}
		close(fd[1]);
	}
	else {
		printf("\nWe are now in Parent Process\n");
		close(fd[1]); 
		char str2[50];
		if(read(fd[0], str2, sizeof(char[50])) == -1) {
			printf("\nAn error occured while reading from the pipe\n");
			return -1;
		} else {
			printf("\nMessage from Child Process : \n%s\n", str2);
		}
		close(fd[0]);
	}
	return 0;
}