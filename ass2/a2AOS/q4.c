#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<time.h>

int main() {
        int fd1[2]; // child => parent
        int fd2[2]; // parent => child
        if(pipe(fd1) == -1) {
                return -1;
        }
        if(pipe(fd2) == -1) {
                return -1;
        }
        int pid = fork();
        if(pid == -1) {
                printf("\nFailed to create child process!!\n");
                return -1;
        }
        if(pid == 0) {
                // child process
                close(fd1[0]);
                close(fd2[1]);
                int x;
                if(read(fd2[0], &x, sizeof(x)) == -1) {
                        printf("an error occured while reading in child\n");
                        return -1;
                }
                printf("Recieved from parent : %d\n", x);
                x *= x;
                if(write(fd1[1], &x, sizeof(x)) == -1) {
                        printf("an error occured while writting in child\n");
                        return -1;
                }
                printf("send to parent!\n");
                close(fd1[1]);
                close(fd2[0]);
        } else {
                // parant process
                close(fd1[1]);
                close(fd2[0]);
                srand(time(NULL));
                int y = rand() % 10;
                if(write(fd2[1], &y, sizeof(y)) == -1) {
                        printf("an error occured while writing in parent\n");
                        return -1;
                }
                printf("x = %d\nsend to child\n", y);
                if(read(fd1[0], &y, sizeof(y)) == -1) {
                        printf("an error occured while reading in parent!!\n");
                        return -1;
                }
                printf("\nFinal Result is %d\n", y);
                close(fd1[0]);
                close(fd2[1]);
        }
        return 0;
}


/* output

arti% ./a.out 
x = 2
send to child
Recieved from parent : 2
send to parent!

Final Result is 4
*/