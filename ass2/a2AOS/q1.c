#include <unistd.h> 
#include <sys/types.h> 
#include <stdio.h> 
#include <fcntl.h> 

char string[] = "hello";

int main() {

    int i;
    char buff[1024];
    char *cp1, *cp2;

    int fds[2];

    cp1 = string;
    cp2 = buff;

    while(*cp1)
        *cp2++ = *cp1++;

    pipe(fds);

    for(i=0;i<10;i++) {

        write(fds[1], buff, 6);
        read(fds[0], buff, 6);
        printf("%s",buff);
    }    
}

/* output

arti% ./a.out 
hellohellohellohellohellohellohellohellohellohello%                                   
*/