#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>

void getInode(int n, char **arr1){
        for(int i=1 ; i<n ; i++) {
                int pid = fork();
                if(pid < 0) {
                        printf("Unable to create process!!");
                        return;
                }
                if(pid > 0) {
                        execlp("/bin/ls", "ls", "-i", arr1[i], NULL);
                }
        }
}

int main(int argc, char** argv) {
        if(argc <= 1) {
                printf("try to execute code with file names as CLA\n");
                exit(-1);
        }
        getInode(argc, argv);
}

/*  output 

./a.out q2.c q1.c
61473743 q2.c
61473742 q1.c

*/