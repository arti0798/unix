#include<stdio.h> 
long int calsize(char *fname) 
{ 
	// printf("fname : *****  %s\n",fname);
	FILE* fp=fopen(fname,"r"); 
	if(fp==NULL) 
	{ 
		printf("File not Found"); 
		return -1; 
	} 
	fseek(fp,0L,SEEK_END); 
	long int res=ftell(fp); 
	fclose(fp); 
	return res; 
} 
int main(int argc,char *argv[]) 
{ 
	int i,arr[20],j; 
	char *name[20]; 
	if(argc>=2) 
	{ 
		for(i=1;i<argc;i++) 
		{ 
			arr[i]=calsize(argv[i]); 
			name[i]=argv[i];

			printf("\n*****size : %d, name : %s\n",arr[i],name[i]); 
		} 
	} 
	printf("\n Files Before Sorting\n"); 
	for(i=1;i<argc;i++) 
		printf("%d\t%s\n",arr[i],name[i]); 
	
	int l=sizeof(arr)/sizeof(int); 
	int min; 
	char *b;
       	
	for(i=1;i<argc;i++) 
	{ 
		for(j=i+1;j<argc;j++) 
		{ 
			if(arr[j]<arr[i]) 
			{ 
				min=arr[i]; 
				arr[i]=arr[j]; 
				arr[j]=min; 
				b=name[i]; 
				name[i]=name[j]; 
				name[j]=b; 
			} 
		} 
	}
       	printf("\n Files after Sorting\n"); 
	for(i=1;i<argc;i++) 
		printf("%d\t%s\n",arr[i],name[i]); 
		
	return 0; 
}


/*  output 


 Files Before Sorting
1610    q3.c
1968    q5.c
1615    q4.c
171     log.txt

 Files after Sorting
171     log.txt
1610    q3.c
1615    q4.c
1968    q5.c

*/