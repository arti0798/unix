#include<stdio.h>
#include<sys/time.h>
#include<stdlib.h>
#include<sys/types.h>
#include<fcntl.h>
#include<sys/stat.h>

int main(int argc, char *argv[]) {

    int check;
    if(argc !=2) {
        printf("Accept only 2 arguments");
        exit(1);
    }
    check = stat(argv[1], &statbuff);

    if(check == 0) {

        if((statbuff.st_mode & S_IRGRP) == S_IRGRP) 
            printf("Group has read permission\n");

        if((statbuff.st_mode & S_IWGRP) == S_IWGRP) 
            printf("Group has write permission\n");

        if((statbuff.st_mode & S_IXGRP) == S_IXGRP) 
            printf("Group has excute permission\n");
    
   
    }
}