#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
int main() {
        int fd[2]; // index 0 for read and 1 for write
        system("clear");
        if(pipe(fd) == -1) {
                printf("An error occured while opening an pipe");
                return -1;
        }
        char str1[50];
        printf("Enter an message to send : ");
        scanf("%[^\n]s", str1);
        getchar();
        if(write(fd[1], str1, sizeof(char[50])) == -1) {
                printf("\nAn error occured while writing to the pipe\n");
                return -1;
        } else {
                printf("\nMessage sent!!!\n");
     }
        char str2[50];
        if(read(fd[0], str2, sizeof(char[50])) == -1) {
                printf("\nAn error occured while reading from the pipe\n");
                return -1;
        } else {
                printf("\nCHILD : Message Recieved : %s\n", str2);
        }
     return 0;
}

/* OUTPUT


Enter an message to send : This message is from parent

Message sent!!!

CHILD : Message Recieved : This message is from parent

*/