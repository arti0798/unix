// #include<stdio.h>
// #include<stdlib.h>
// #include<dirent.h>


#include<stdio.h>
#include<dirent.h>



int main(void)
{
    DIR *d;
    struct dirent *dir;
    int count = 0;
    d = opendir(".");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            printf("%s\n", dir->d_name);
            count++;
        }
        closedir(d);
    }
    printf("Count : %d\n",count);
    return(0);
}


/*OUTPUT

arti@arti:~/unix/signal$ ./a.out 
q20.c
q1.c
q19.c
.
Question.txt
a.out
..

*/

