#include<stdio.h>
#include<dirent.h>
int main(void)
{
    DIR *d;
    struct dirent *dir;
    char path[100];
    printf("Enter directory path:");
    scanf("%s",path);
    d = opendir(path);
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            printf("%s\n", dir->d_name);
        }
        closedir(d);
    }
    return(0);
}

/*OUTPUT


Enter directory path:/home/arti/unix/dirChecker    
a2.txt
.
..
a1.txt

*/