
#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<fcntl.h>

int main(int argc,const char *argv[]){
    char random_text[13] = "Hello World !";
    int fp = -1;
    int i = 0;
    if(argc<2){
        printf("\nPlease provide file");
        return 0;
    }
    fp = open(argv[1],O_WRONLY|O_CREAT);
    if(fp<0){
        printf("Can't open the file..");
        return 0;
    }
    while(i<5){
    write(fp,random_text,13);
    lseek(fp,100,SEEK_CUR);
    i++;
    printf("\nWriting done for %d times",i-1);
    }
    close(fp);
    return 0;
}

/*

arti% ./a.out q14.c

Writing done for 0 times
Writing done for 1 times
Writing done for 2 times
Writing done for 3 times
Writing done for 4 times% 
*/