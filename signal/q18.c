#include<stdio.h>
#include<dirent.h>

int main(void)
{
    DIR *d;
    struct dirent *dir;
    char str[20];
    printf("Enter directory name : \t");
    scanf("%s",str);
    d = opendir(str);
    // d = opendir(".");
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            printf("%s\n", dir->d_name);
        }
        closedir(d);
    }
    return(0);
}

/*

arti% ./a.out 
Enter directory name :  test
.
..
a.txt

*/