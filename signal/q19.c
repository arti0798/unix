#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<dirent.h>
#include<string.h>
#include<sys/wait.h>

void typeline(char c2[], char *fname) {
	FILE *fp = NULL;
	fp = fopen(fname, "r");
	int cnt = 0;
	int total = 0;
	if(fp == NULL) {
		printf("Unable to open file!!\n");
		return;
	}
	//printf("Hello");
	char ch[50],*num;
	if(c2[0] == '+') {
		num = strtok(c2,"+");
		int n = atoi(num),i=0;
		while(fgets(ch,50,fp) && i<n) {
			printf("%d\t%s", (i+1), ch);
			i++;
		}
	}
	if(c2[0] == '-') {
		int i=0;
		
		while(fgets(ch,100,fp)) {
			i++;
		}
		total = i;
		int n = atoi(c2)*-1;
		
		fseek(fp,0,SEEK_SET);
		i =0;
		while(fgets(ch,100,fp) && i<(total-n)-1) {
			
			i++;
		}

		while(fgets(ch,100,fp) && !feof(fp)) {
			printf("%d\t%s",(i+1), ch);
			i++;
		}

	}
	if(c2[0] == 'a') {
		while(fgets(ch,50,fp)) {
			printf("%s", ch);
		}
	}
	fclose(fp);
}

int main() {
	char buf[100];
	system("clear");
	while(1) {
		printf("\nModernShell$ ");
		scanf("%[^\n]s", buf);
		
		getchar();
		char c1[30], c2[30], fname[30];
		int cnt = sscanf(buf, "%s %s %s", c1, c2, fname);
		
		if(strcmp(c1, "exit")==0) {
			exit(0);
		}
        else if(cnt == 3) {
			if(strcmp(c1,"typeline")==0)
				typeline(c2, fname);
		}
		else
			printf("\nSyntax error..... please check and try again!\n");

		
	}
}

/* OUTPUT 


ModernShell$ typeline +5 Question.txt
1       1. Write a program to create a named pipe where c2      hild sends message to parent.
3
4       2. Write a program to create an unnamed pipe wher5      e parent sends message to child.

ModernShell$ typeline -20 Question.txt
44      15. Print the type of file and group permissions of it where file name is accepted thru command lin45  e arguments.
46
47      16. Write a program to catch death of child signal by parent process after 5 seconds.
48
49      Use alarm system call
50
51      17. Write a program where parent and child share file access
52
53      18. Write the program that accepts directory name as input and print its contents
54
55      19. Write a program to implement following commands in linux.
56
57      Typelines +5 <filename> print first 5 lines of a file.
58
59      Typelines -8 <filename>print last 20 lines of a file.
60
61      20. Write a program to count the no. of files of a directory.
62


*/