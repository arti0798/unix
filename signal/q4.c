
#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<unistd.h>
void INTHandler()
{
        static int cnt = 0;
        cnt++;
        printf("\n SIGINT signal occured %d times.\n", cnt);
        if(cnt >= 5)
                signal(SIGINT,SIG_DFL);
}

void TSTPHandler()
{
        printf("\n Terminal stop signal has occured!\n");
        signal(SIGTSTP, TSTPHandler);
}

void main()
{
        int cnt = 0;
        signal(SIGINT, INTHandler);
        signal(SIGTSTP, TSTPHandler);
        while(1)
        {
                sleep(1);
        }
}


/* OUTPUT


arti% ./a.out 
^C
 SIGINT signal occured 1 times.
^C
 SIGINT signal occured 2 times.
^C
 SIGINT signal occured 3 times.
^C
 SIGINT signal occured 4 times.
^C
 SIGINT signal occured 5 times.
^C

*/