  #include<stdio.h> 
  #include<stdlib.h> 
  #include<sys/types.h> 
  #include<fcntl.h> 
  #include<sys/stat.h> 
  main(int argc, char *argv[]){

    struct stat statbuff; int check; if(argc!=2){

        printf("Can accept only two arguments");
        exit(1);
    }
    check=stat(argv[1], &statbuff); 
    if(check==0){

        if(S_ISREG(statbuff.st_mode))
            printf("Regular File");
        else if(S_ISDIR(statbuff.st_mode)) 
            printf("Directory");
        else
             printf("Other File");
    }
    if((statbuff.st_mode & S_IRUSR)==S_IRUSR)
        printf("\n Owner has Read Permission\n"); 
    if((statbuff.st_mode & S_IWUSR)==S_IWUSR)
        printf("Owner has Write Permission\n"); 
    if((statbuff.st_mode & S_IXUSR)==S_IXUSR)
        printf("Owner has Execute Permission\n");


    }

    /* OUTPUT

    arti% ./a.out q14.c
Regular File
 Owner has Read Permission
Owner has Write Permission

*/