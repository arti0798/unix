1. Write a program to create a named pipe where child sends message to parent.

2. Write a program to create an unnamed pipe where parent sends message to child.

3. Write a program in C to print the file names of a specified directory.

4. Write a program to catch SIGINT signal five times and print message ‘SIGINT signal occurred’ every time and exit at sixth occurrence. Also ignore every occurrence of SIGTSTP signal.

5. Write a program to block SIGINT signal for 5 seconds.

6. Write a program in LINUX to simulate extended shell. 
Show the prompt and accept standard shell command, 
which will be executed by child process using one of the exec 
family system calls. 
Parent process waits until child finishes execution. 
The command may consist of at the most 5 parameters. 
The process should be repeated till user types “exit”.

7. Write a program to count the no. of files of a specified directory.

8. Use of dup and dup2 system call. Create one file named temp.txt. Write some contents in the file. Make use of dup and dup2 system calls to write following contents in the file. Make use of four descriptorsand channel zero.

‘M.Sc. Computer Science

Semester–II Class

Advanced Operating Systems

Practical Examination’

9. Check whether the specified file is regular file or directory and print user permissions of the file.

10. Write a program in linux to block SIGQUIT signal for 5 seconds.

11. Write program to handle SIGINT, SIGSEGV, SIGQUIT and SIFTSTP signals

12. Write program to handle SIGINT, SIGALRM and SIFTSTP signals.

13. Write a program to create hole in it.(Use Lseek system call) .Write “Hello World !” five times after every 100 blank characters.

14. Write program that illustrates suspending and resuming processes using signals

15. Print the type of file and group permissions of it where file name is accepted thru command line arguments.

16. Write a program to catch death of child signal by parent process after 5 seconds.

Use alarm system call

17. Write a program where parent and child share file access

18. Write the program that accepts directory name as input and print its contents

19. Write a program to implement following commands in linux.

Typelines +5 <filename> print first 5 lines of a file.

Typelines -8 <filename>print last 20 lines of a file.

20. Write a program to count the no. of files of a directory.

21. Print the type of file where file name is accepted thru command line arguments.