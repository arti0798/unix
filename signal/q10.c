#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<stdlib.h>

//signal handling function that will except ctrl-\ and ctrl-c
void sig_handler(int signo)
{
        printf("\nSIGQUIT receivedT\n");
        sleep(5);
        exit(0);
}

int main(void)
{
    signal(SIGQUIT, sig_handler);
    while(1){
        printf("\nTrying to block for 5 sec");
        sleep(1);
        
    }
    return 0;
}