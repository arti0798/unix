#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include<unistd.h>
void INTHandler()
{
        static int cnt = 0;
        cnt++;
        printf("\n INTerrupt Signal has occured %d times.\n", cnt);
        if(cnt >= 5)
                signal(SIGINT,SIG_DFL);
}

void TSTPHandler()
{
        printf("\n Terminal stop signal has occured!\n");
        signal(SIGTSTP, TSTPHandler);
}

void main()
{
        int cnt = 0;
        signal(SIGINT, INTHandler);
        signal(SIGTSTP, TSTPHandler);
        while(1)
        {
                printf("Hello we are counting signal here ! \n");
                sleep(1);
        }
}
