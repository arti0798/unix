#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
int main() {

    int pid;            //process id
    pid = fork();     //create another process
    if ( pid < 0 ) {
                                         //fail
        printf("\nFork failed\n");
        exit (-1);
    }
    else if( pid == 0 ) {                     //child

        execlp ( "/bin/ls", "ls", "-l", NULL );  //execute ls
    }
    else {    
                                 //parent
        wait (NULL);              //wait for child
        printf("\nchild complete\n");
        exit (0);
    }
}

//------------------------------------------------

/*

total 48
-rwxrwxr-x 1 arti arti 16872 Jul 11 22:39 a.out
-rw-rw-r-- 1 arti arti  1695 Jul 11 22:26 birthday.txt
-rw-rw-r-- 1 arti arti   367 May 24 17:30 chapter1.txt
-rw-rw-r-- 1 arti arti   526 Jul 11 22:39 ls_l_filename2.c
-rw-rw-r-- 1 arti arti  1298 Jul 11 22:24 ls_space_l_command.c
-rwx------ 1 arti arti   224 Jul 11 21:41 op.txt
-rwx------ 1 arti arti   277 Jul 11 22:23 output.txt
-rw-rw-r-- 1 arti arti  1305 Jul 11 22:26 substring.txt

child complete
*/