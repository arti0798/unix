#include<stdio.h>
#include<stdlib.h>

struct node {

    int key;
    struct node *right;
    struct node *left;

};
struct node * getNode(int value) {

    struct node *temp = malloc(sizeof(struct node *));
    temp->key = value;
    temp->right = NULL;
    temp->left = NULL;
    return temp;
}
struct node * insert(struct node *root, int value) {

    if(root == NULL) {
        return getNode(value);
    }
    else if(value < root->key)
            root->left = insert(root->left,value);
    else if(value > root->key) 
        root->right = insert(root->right,value);

    return root;            
}
void preOrder(struct node *root) {

    if(root == NULL)
        return;

    printf("%d----",root->key);
    preOrder(root->left);
    preOrder(root->right);    
}
void inorder(struct node *root) {

    if(root == NULL)
        return;
    inorder(root->left);
    printf("%d---",root->key);
    inorder(root->right);    
}
int main() {

    struct node *root = NULL;

   root = insert(root,10);
   preOrder(root);
}