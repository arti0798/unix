#include<stdio.h>
#include<stdlib.h>

struct node {

    int info;
    struct node *next;
};
struct node *getNode(int value) {

    struct node *temp = NULL;
    temp = malloc(sizeof(struct node *));
    temp->info = value;
    temp->next = NULL;
    return temp;
}
void init(struct node **h) {

    *h = getNode(0);
}
void append(struct  node *h, int value) {

    struct node * p = h;
    struct node *temp = getNode(value);

    while(p->next!=NULL) {

        p = p->next;
    }
    p->next = temp;
    h->info++;
}

void insert(struct node * h,int pos,int value) {

    struct node *p = h;
    struct node *q = h->next;
    struct node *temp = getNode(value);

    int i =0;

    while(i<pos-1) {

        p = q;
        q = q->next;
        i++;

    }

    p->next = temp;
    temp->next = q;
    h->info++;
}
void delete(struct node *h, int pos) {

    struct node *p = h;
    struct node *q = h->next;
    struct node *temp = NULL;
    int i = 0;

    while(i < pos-1) {

        p = q;
        q = q->next;
        i++;
    }
    temp = q;
    q = q->next;
    p->next = q;
    temp->next = NULL;
    free(temp);
    h->info--;


}
int main() {

    struct node *head = NULL;

    init(&head);

    int value = 10;
    append(head,value);
    insert(head,3,value);
    delete(head,3);
}