#include <sys/types.h>
// unistd.h : header defines miscellaneous symbolic constants and types, 
//and declares miscellaneous functions.
#include <unistd.h>
// header defines the structure of the data returned by the functions fstat(), lstat(), and stat().
#include <sys/stat.h>

int main() {

    int process_id=fork(); //The fork() function is used to create a new process by duplicating the existing process
    int fd=creat("output.txt",S_IRWXU); 
    //this is equivalent to ‘(S_IRUSR (Read permission bit for the owner of the file)| S_IWUSR (Write permission bit for
    //  the owner of the file)| S_IXUSR (Execute (for ordinary files) or search (for directories) permission bit for the owner of the file.))’.
    if(process_id!=0 && process_id!=-1) {
        close(1); // stdout
        dup(fd); // replace file descriptor
                    // successful, dup() returns a new file descriptor.
        execlp("ls","ls","-l",NULL);
        close(fd);

    }
}



//-------------outout.txt--------------
/*total 32
-rwxrwxr-x 1 arti arti 16880 Jul 11 22:22 a.out
-rw-rw-r-- 1 arti arti   367 May 24 17:30 chapter1.txt
-rw-rw-r-- 1 arti arti  1433 Jul 11 22:22 ls_space_l_command.c
-rwx------ 1 arti arti   224 Jul 11 21:41 op.txt
-rwx------ 1 arti arti     0 Jul 11 22:23 output.txt


*/