#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<dirent.h>
#include<string.h>
#include<sys/wait.h>

void typeline(char c2[], char *fname) {
	FILE *fp = NULL;
	fp = fopen(fname, "r");
	int cnt = 0;
	int total = 0;
	if(fp == NULL) {
		printf("Unable to open file!!\n");
		return;
	}
	//printf("Hello");
	char ch[50],*num;
	if(c2[0] == '+') {
		num = strtok(c2,"+");
		int n = atoi(num),i=0;
		while(fgets(ch,50,fp) && i<n) {
			printf("%d\t%s", (i+1), ch);
			i++;
		}
	}
	if(c2[0] == '-') {
		int i=0;
		
		while(fgets(ch,100,fp)) {
			i++;
		}
		total = i;
		int n = atoi(c2)*-1;
		
		fseek(fp,0,SEEK_SET);
		i =0;
		while(fgets(ch,100,fp) && i<(total-n)-1) {
			
			i++;
		}

		while(fgets(ch,100,fp) && !feof(fp)) {
			printf("%d\t%s",(i+1), ch);
			i++;
		}

	}
	if(c2[0] == 'a') {
		while(fgets(ch,50,fp)) {
			printf("%s", ch);
		}
	}
	fclose(fp);
}

int main() {
	char buf[100];
	system("clear");
	while(1) {
		printf("\nModernShell$ ");
		scanf("%[^\n]s", buf);
		
		getchar();
		char c1[30], c2[30], fname[30];
		int cnt = sscanf(buf, "%s %s %s", c1, c2, fname);
		
		if(strcmp(c1, "exit")==0) {
			exit(0);
		}
        else if(cnt == 3) {
			if(strcmp(c1,"typeline")==0)
				typeline(c2, fname);
		}
		else
			printf("\nSyntax error..... please check and try again!\n");

		
	}
}

/* OUTPUT 

ModernShell$ typeline +10 log.txt
1       [LOG] child proccess terminated.
2
3       aaa
4       bbb
5       cbc
6       ffff
7       7th line
8       8th line
9       9th line
10      10th line

ModernShell$ typeline -20 log.txt
11
12      asd
13      asdg
14      hhh
15      jjj
16      ttt
17      444
18      yyyh
19      55r
20      444
21      6767
22      uujh6
23
24      66
25      6
26      6
27      444
28      3
29      2w2w2
30

ModernShell$ typeline a log.txt
[LOG] child proccess terminated.

aaa
bbb
cbc
ffff
7th line
8th line
9th line
10th line
11 thn line

asd
asdg
hhh
jjj
ttt
444
yyyh
55r
444
6767
uujh6

66
6
6
444
3
2w2w2


*/