#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<dirent.h>
#include<string.h>
#include<sys/wait.h>

void foccur(char *pattern, char *fname) {

    // printf("\nI m in foccur\n");
	FILE *fp = fopen(fname, "r");
	if(fp == NULL) {
		printf("Can't open file :(");
		return ;
	}
	char ch[50];
	int i=0;
	while(!feof(fp))
	{
		fscanf(fp,"%s",ch);
		i++;
		if(strcmp(ch,pattern)==0)
		{
			printf("\nFirst Occurance found on line no %d\n",i);
			return;
		}
	}

	fclose(fp);
}
void count(char *pattern, char *fname) {
	FILE *fp = fopen(fname, "r");
	if(fp == NULL) {
		printf("Can't open file :(");
		return ;
	}
	char ch[50];
	int i=0;
	while(!feof(fp))
	{
		fscanf(fp,"%s",ch);
		if(strcmp(ch,pattern)==0)
			i++;
	}
	printf("\nNo of occurances = %d\n",i);

}
void aoccur(char *pattern,char *fname) {
	FILE *fp = fopen(fname, "r");
        if(fp == NULL) {
                printf("Can't open file :(");
                return ;
        }
        char ch[50];
	int i=0;
	printf("Displaying all the occurances of %s are\n",pattern);
	while(!feof(fp))
	{
		fscanf(fp,"%s",ch);
		if(strcmp(ch,pattern)==0)
			printf("%s\n",ch);
	}

}
int main() {
	char buffer[130];
	system("clear");
	
	while(1) {
		printf("\nModernShell$ ");
		scanf("%[^\n]s", buffer);
		getchar();
		
		char c1[30], c2, pattern[30], fname[30];
		int cnt = sscanf(buffer, "%s %c %s %s", c1, &c2, pattern, fname);
		if(strcmp(c1, "exit") == 0) {
			printf("\nbye.....\n");
			exit(0);
		}
        // printf("\n***%c\n",c2);
		switch(c2) {
			case 'f': foccur(pattern, fname);
				  break;
			case 'c': count(pattern, fname);
				  break;
			case 'a': aoccur(pattern, fname);
				  break;
			default : printf("Invalid Command!");
		}
	}
}


/*  OUTPUT

ModernShell$ search f aaa log.txt

First Occurance found on line no 5

ModernShell$ search c bbb log.txt

No of occurances = 1

ModernShell$ search a cbc log.txt
Displaying all the occurances of cbc are
cbc

*/