#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/wait.h>

void count(char c2, char *fname) {
	printf("%c", c2);
	FILE *fp = NULL;
	printf("%s", fname);
	fp = fopen(fname, "r");
	if(fp == NULL) {
		printf("Unable to open file!!");
		return;
	}
	char ch;
	int cnt = 0;
	if(c2 == 'c') {
		while((ch = fgetc(fp)) != EOF) {
			if(ch == ' ' || ch == '\n'){
				cnt++;
			}
		}
		printf("\ncharacter count in file %s is %d\n", fname, cnt);
	}
	else if(c2 == 'w') {
		while((ch = fgetc(fp)) != EOF) {
			if(ch == ' ' || ch == '\n'){
				//printf("%c", ch);
				cnt++;
			}
		}
		printf("\nword count in file %s is %d\n", fname, cnt);          
	}
	else if(c2 == 'l') {
		while((ch = fgetc(fp)) != EOF) {
			if(ch == '\n'){
				//printf("%c", ch);
				cnt++;
			}
		}
		printf("\nline count in file %s is %d\n", fname, cnt);          
	}
	else{
		printf("Invalid Command!!");
	}
	fclose(fp);
}
int main() {
	char buffer[100];
	system("clear");
	while(1) {
		printf("ModernShell$ ");
		scanf("%[^\n]s", buffer);
		getchar();
		
		char c1[30], c2, fname[30];
		int cnt = sscanf(buffer, "%s %c %s", c1, &c2, fname);
		
		if(strcmp(c1, "exit")==0) {
			exit(0);
		}
		// printf("\n******%d\n",cnt);
		if(cnt == 3) {
			if(strcmp(c1,"count")==0)
				count(c2, fname);
		}
		else
			printf("\nSyntax error..... please check and try again!\n");

		
	}
}

/* OUTPUT

ModernShell$ count c log.txt
clog.txt
character count in file log.txt is 40
ModernShell$ count w log.txt
wlog.txt
word count in file log.txt is 40
ModernShell$ count l log.txt
llog.txt
line count in file log.txt is 31

*/